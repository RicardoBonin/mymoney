import React, { useEffect, useState } from 'react'
import { Redirect} from 'react-router-dom'
import { usePost } from './utils/rest'

const url ='https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyD9LCNW1Uhkr00j6cStnbH4qLGowehJsHE'

const Login = () => {
    const [postData, signin] = usePost(url)
    const [logado, setLogado] = useState(false)
    const [email, setEmail] = useState('')
    const [senha, setSenha] = useState('')
    useEffect(() => {
        console.log(postData)
        if (Object.keys(postData.data).length > 0) {
            localStorage.setItem('token', postData.data.idToken)
            window.location.reload()
        }
    }, [postData])
    useEffect(() => {
        const token = localStorage.getItem('token')
        if (token) {
            setLogado(true)
        }
    })
    const login = async () => {
        await signin ({
            email,
            password: senha,
            returnSecureToken: true
        })
    }
    const onChangeEmail = evt => {
        setEmail(evt.target.value)
    }
    const onChangeSenha = evt => {
        setSenha(evt.target.value)
    }
    if (logado ) {
        return <Redirect to='/' />
    }
    return(
        <div className='container border rounded p-5 mt-5 w-25 d-flex justify-content-center'>
            
            <form>
                <h1 className='text-center'>Login</h1>
                {
                    postData.error && postData.error.length > 0 &&
                    <p>E-mail e/ou senha inválidos.</p>
                }
                <div className='form-group'>
                    <label htmlFor='exampleInputEmail1'>Endereço de email</label>
                    <input type='email' className='form-control' id='exampleInputEmail1' value={email} onChange={onChangeEmail} aria-describedby='emailHelp' placeholder='Seu email' />
                </div>
                <div className='form-group'>
                    <label for='exampleInputPassword1'>Senha</label>
                    <input type='password' className='form-control ' id='exampleInputPassword1' value={senha} onChange={onChangeSenha} placeholder='Senha' />
                </div>
                <button type='button' className='btn btn-primary w-100' onClick={login}>Login</button>
            </form>
        </div>   
    )
}

export default Login